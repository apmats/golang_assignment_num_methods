package main

import (
	"fmt"
	"math"
	"mathFramework/singleVarFuncMethods"
)


func main() {
	minimumAt, minimumValue, _ := singleVarFuncMethods.MinimizeFunctionWithinInterval(xsquaredplusxplusfive, -10, 10, 0.000001)
	fmt.Printf("Minimum is %.5f at x = %.5f\n", minimumValue, minimumAt)

	integral, _ := singleVarFuncMethods.IntegrateFunctionWithinInterval(xsquaredplusxplusfive, -10, 10)
	fmt.Printf("Integral is %.5f\n", integral)
}

func xsquaredplusxplusfive(x float64) float64 {
	return math.Pow(x, 2) + x + 5
}
