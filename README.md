## Assignment 2: 

Numerical methods framework

You are building a Python or Go framework that will allow applying a number of numerical solution algorithms to arbitrary functions of a single variable

* Provide the main interfaces that make up the top levels of the system
* Implement numerical methods for performing the following, on an arbitrary function of a single variable:
- Function minimization
- Integration of a function

Describe the main performance considerations when implementing numerical techniques in Python
Make suggestions to improve performance


### Notes: 

We're implementing part of a supposed framework for numerical methods over functions. Intended usage is importing our package and calling one of the exported functions, passing it a function that conforms to the type that the exported function expects. The only type we currently have is a SingleVarFunc type which is a function that takes a float64 and returns one, but depending on what other functionality our package would eventually implement, there could be a few other such types (for other numerical inputs for examples, or for extending to multiple variable functions etc.) 

My package provides functions for numerical methods for single variable functions that take a float64 as input and provide one as output.
The implementation for the two methods requested can be found under the singleVarFuncMethods.go file.
They both work on continuous function, over a specific interval, and in the case of looking for minima, we also expect the function to be unimodal.

I'm not sure what the performance considerations would be in such a case. Obviously, as I did in my implementation of the integration method, for any method that can use pregenerated data instead of computing them again should do so. The Gauss quadrature method should hold static arrays for predetermined degree polynomials and only resort to computing them in case the caller requests a higher degree than the stored ones.
We'd also have to figure the use case of our framework and decide upon the numerical types we'd use. My implemented functions take float64 -> float64 functions as input, however our application could demand use of more accurate floating point numbers, or larger numbers, thus we'd have to use math/big and possibly take a performance hit for simpler applications.

Performance in the case of such a library doesn't only involve better resource utilisation, but providing results that are closer to the expected result of the mathematical algorithm 
When dealing with floating numbers, we are always dealing with some sort of rounding error, so it's a good idea to try to minimize processing of our data that might introduce more of it into your numbers. Avoid big loops of touching numbers or minimize them when expressing mathematical algorithms as code.
If the algorithm we are transfering to code requires checking whether an equality holds between floating point numbers, use of an epsilon value for the difference between those 2 numbers instead of checking for the equality is a must, because values that we'd expect to be the same mathematically end up being rounded and stored as different values.

Apart from these, general optimization techniques should be used in any performance-critical part of our code. That involves calls to fine-tuned assembly/c code instead of go code (so we should use all the functions that the math lib provides instead of writing our own, since those calls are run from assembly files, supposedly optimized), we should check whether our smaller functions are being inlined by the compiler and trying to get them in a state that they will be if they are called frequently enough, manually unroll loops which we know to be performance-critical etc.

Run demo.go to see a demonstration of the minimum and integral functions.
